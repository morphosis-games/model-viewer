var path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CopyPlugin = require('copy-webpack-plugin');
// const WorkboxPlugin = require('workbox-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PRODUCTION = false; // process.env.NODE_ENV === 'production';

let plugins = [
  new CopyPlugin([
    // { from: 'src/browserconfig.xml', to: 'browserconfig.xml' },
    // { from: 'src/manifest.webmanifest', to: 'manifest.webmanifest' },
    // { from: 'src/assets', to: 'assets' },
  ]),
  new HtmlWebpackPlugin({
    template: "src/index.html",
  }),
  new VueLoaderPlugin(),
];

const config = {
  node: {fs: "empty"},
  plugins,
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './www/'),
    publicPath: './',
    filename: 'build.js',
    globalObject: "this",
  },
  module: {
    rules: [
      {test: /\.vue$/, loader: 'vue-loader'},
      {test: /\.less$/, loader: 'less-loader', options: {javascriptEnabled: true}},
      // {test: /\.worker.js$/, loader: 'worker-loader'},
      {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/, options: {
        "presets": [
          ["@babel/preset-env", {
            "useBuiltIns": "usage",
            "corejs": 3,
            "modules": false,
            "targets": {
              "chrome": 72,
            }
          }],
        ],
        "plugins": [
          "@babel/plugin-syntax-dynamic-import"
        ]
      }},
      {test: /\.css/, loaders: ['style-loader', 'css-loader']},
      {test: /\.(png|jpg|gif|svg|woff|woff2|ttf|eot)$/, loader: 'file-loader', options: {name: '[name].[ext]?[hash]'}},
    ]
  },
  resolve: {
    alias: {
      'vue$': path.resolve('./node_modules/vue/dist/vue.runtime.esm.js'),
      '@morphosis/vue-websocket': path.resolve('../vue-websocket/'),
      '@morphosis/tsunami-node': path.resolve('../tsunami-node/'),
      '@morphosis/adventure-navigator': path.resolve('../adventure-navigator/'),
    },
    extensions: ['*', '.js', '.vue', '.json'],
    symlinks: false,
  },
  devServer: {
    publicPath: "/",
    contentBase: path.resolve(__dirname, './www/'),
    // historyApiFallback: true,
    // noInfo: true,
    // overlay: true,
  },
  performance: {
    hints: false
  },
  devtool: '#source-map',  // PRODUCTION ? false : '#source-map',
  mode: PRODUCTION ? "production" : "development"
}

module.exports = config;