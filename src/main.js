import Vue from 'vue';
import App from './app.vue';

new (Vue.extend(App))({
  el: '#app'
});
